package learn

/**
 * Esta es la primera clase en kotlin
 * */
fun main(args: Array<String>) {
    // CREAR CONSTANTES
    val greeting="Buenos Dias"
    var name: String
    var age:Int

    //Usando inferencia de tipos

    var lastname="Garcia"

    // asignar valores a variables creadas
    name="Luis F."
    age=24
    // println
    println("Hello World!")
    println(greeting+" "+name+" "+lastname+", por sus "+age+" años")
}
